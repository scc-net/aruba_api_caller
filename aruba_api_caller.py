import json
import requests
import sys
import time
import urllib.parse


class api_session:
    def __init__(self, api_url, username, password, port=4343, ssl=True, check_ssl=True, verbose=False, retrys=3,
                 retry_wait=0.5):
        if ssl:
            protocol = "https"
        else:
            protocol = "http"

        self.session = None
        self.api_token = None
        self.api_url = "{}://{}:{}/v1/".format(protocol, api_url, port)
        self.username = username
        self.password = password
        self.check_ssl = check_ssl
        self.verbose = verbose
        self.retrys = retrys
        self.retry_wait = retry_wait

    def login(self):
        self.session = requests.Session()
        for i in range(1, self.retrys + 1):
            if self.verbose:
                print("Verbose: login, try {}".format(str(i)))
            # use url encode because content type x-www-form-urlencoded
            response = self.session.post(
                "{}api/login".format(self.api_url),
                verify=self.check_ssl,
                data="username={}&password={}".format(urllib.parse.quote_plus(self.username),
                                                      urllib.parse.quote_plus(self.password)))
            login_data = self.parse_response(response)
            if self.verbose:
                print("Verbose: {}".format(login_data["_global_result"]["status_str"]))
            if login_data["_global_result"]["status"] == "0":
                self.api_token = login_data["_global_result"]["UIDARUBA"]
                return
            if i == self.retrys:
                if self.verbose:
                    print("There was an Error with the login. Please check the credentials.", file=sys.stderr)
                    print("Controller-IP: {}, Username: {}".format(self.api_url, self.username), file=sys.stderr)
                raise PermissionError(
                    "There was an Error with the login. Please check the credentials of the User >{}< at host >{}<".format(
                        self.username, self.api_url))
            time.sleep(self.retry_wait)

    def logout(self):
        response = self.session.get(self.api_url + "api/logout")
        logout_data = self.parse_response(response)
        self.api_token = None
        if self.verbose:
            print("Verbose: {}".format(logout_data["_global_result"]["status_str"]))

    def get(self, api_path, config_path=None):
        if self.api_token is None:
            self.login()
        if config_path is not None:
            node_path = "&config_path={}".format(config_path)
        else:
            node_path = ""
        response = self.session.get("{}{}?UIDARUBA={}{}".format(self.api_url, api_path, self.api_token, node_path))
        data = self.parse_response(response)
        if self.verbose:
            print("\nVerbose: " + str(data))
        return data

    def post(self, api_path, data, config_path="/md"):
        if self.api_token is None:
            self.login()
        response = self.session.post(
            "{}{}?UIDARUBA={}&config_path={}".format(self.api_url, api_path, self.api_token, config_path), json=data)
        data = self.parse_response(response)
        if self.verbose:
            print("Verbose: {}".format(str(data)))
        return data

    def write_memory(self, config_path):
        node_path = "?config_path=" + config_path
        nothing = json.loads('{}')
        response = self.session.post(
            "{}configuration/object/write_memory{}&UIDARUBA={}".format(self.api_url, node_path, self.api_token),
            json=nothing)
        data = self.parse_response(response)
        if self.verbose:
            print("Verbose: {}".format(str(data)))
        return data

    def cli_command(self, command):
        mod_command = command.replace(" ", "+")
        response = self.session.get(
            "{}configuration/showcommand?command={}&UIDARUBA={}".format(self.api_url, mod_command, self.api_token))
        data = self.parse_response(response)
        if self.verbose:
            print("Verbose: {}".format(str(data)))
        return data

    @staticmethod
    def parse_response(res: requests.models.Response):
        res.raise_for_status()
        try:
            data = res.json()
            return data
        except json.decoder.JSONDecodeError:
            if not res.ok and res.text != '':
                raise ValueError(f'No valid json returned: {res.text}')


# function to convert an Uptime from the AP-List into seconds
def convert_uptime(status):
    uptime = 0
    if status.lower().startswith("down"):
        return uptime
    time_array = status.split(" ")[1].split(":")
    for element in time_array:
        time_ = int(element[:-1])
        unit = element[-1:]
        if unit == "s":
            uptime += time_
        elif unit == "m":
            uptime += time_ * 60
        elif unit == "h":
            uptime += time_ * 60 * 60
        elif unit == "d":
            uptime += time_ * 60 * 60 * 24
    return uptime
