#!/usr/bin/env python3

from setuptools import setup

setup(
    name='aruba_api_caller',
    py_modules=['aruba_api_caller'],
    version='0.0.5',
    description='Python module for access to the ArubaOS 8.x REST API.',
    author='Klara Mall',
    author_email='klara.mall@kit.edu',
    url='https://git.scc.kit.edu/scc-net/aruba_api_caller',
    install_requires=[
        'requests',
    ],
    classifiers=[
        # Verhindert versehentliches hochladen auf öffentlichen Repos, z.B.
        # PyPi
        'Private :: Do not upload',
    ],
)
